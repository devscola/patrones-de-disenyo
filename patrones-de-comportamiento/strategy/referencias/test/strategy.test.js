describe("Patrón estrategia", () => {
  it("Cada estrategia implementa los métodos de la interfaz común", () => {
    const addStrategy = new AddStrategy();
    const subtractStrategy = new SubtractStrategy();

    expect(addStrategy.execute()).toBeDefined();
    expect(subtractStrategy.execute()).toBeDefined();
  });

  it("Cada estrategia encapsula el algoritmo a usar", () => {
    const firstNumber = 3;
    const secondNumber = 5;

    const addCalculator = new Calculator(firstNumber, secondNumber);
    const addStrategy = new AddStrategy();
    addCalculator.setStrategy(addStrategy);
    const addStrategyResult = 8;

    const substractCalculator = new Calculator(firstNumber, secondNumber);
    const subtractStrategy = new SubtractStrategy();
    substractCalculator.setStrategy(subtractStrategy);
    const subtractStrategyResult = -2;

    expect(addCalculator.calculate()).toBe(addStrategyResult);
    expect(substractCalculator.calculate()).toBe(subtractStrategyResult);
  });

  it("La estrategia puede recibir parametros del contexto para intercambiar información", () => {
    const firstNumber = 3;
    const secondNumber = 5;
    const calculator = new Calculator(firstNumber, secondNumber);
    const addStrategy = new AddStrategy();
    addStrategy.execute = jest.fn();
    calculator.setStrategy(addStrategy);

    calculator.calculate();

    expect(addStrategy.execute).toHaveBeenCalledWith(firstNumber, secondNumber);
  });

  describe("El cliente puede elegir el algoritmo que le conviene en cada momento", () => {
    let firstNumber;
    let secondNumber;
    let calculator;

    beforeAll(() => {
      firstNumber = 3;
      secondNumber = 5;

      calculator = new Calculator(firstNumber, secondNumber);
    });

    it("El cliente elige sumar", () => {
      const addStrategy = new AddStrategy();
      calculator.setStrategy(addStrategy);

      expect(calculator.calculate()).toBe(8);
      expect(calculator.strategy).toBeInstanceOf(AddStrategy);
    });

    it("El cliente elige restar", () => {
      const subtractStrategy = new SubtractStrategy();
      calculator.setStrategy(subtractStrategy);

      expect(calculator.calculate()).toBe(-2);
      expect(calculator.strategy).toBeInstanceOf(SubtractStrategy);
    });
  });
  it("El cliente puede cambiar de estrategia en runtime", () => {
    const characterLife = new Calculator();
    const drinkWater = new AddStrategy();
    const smoke = new SubtractStrategy();
    characterLife.setStrategy(drinkWater);
    const healthyLife = characterLife.calculate(2, 2);

    characterLife.setStrategy(smoke);
    const unhealthyLife = characterLife.calculate(healthyLife, 2);

    expect(healthyLife).toBe(4);
    expect(unhealthyLife).toBe(2);
  });
});

class Calculator {
  constructor(firstNumber, secondNumber) {
    this.firstNumber = firstNumber;
    this.secondNumber = secondNumber;
  }

  setStrategy(strategy) {
    this.strategy = strategy;
  }

  calculate(firstNumber = this.firstNumber, secondNumber = this.secondNumber) {
    return this.strategy.execute(firstNumber, secondNumber);
  }
}

class InterfaceStrategy {
  execute() {
    throw Error;
  }
}

class AddStrategy extends InterfaceStrategy {
  execute(firstNumber, secondNumber) {
    return firstNumber + secondNumber;
  }
}

class SubtractStrategy extends InterfaceStrategy {
  execute(firstNumber, secondNumber) {
    return firstNumber - secondNumber;
  }
}
