# Calculador de la sensación thermica por viento

## Requirements

    - ruby
    - bundle gem
    - rspec gem

## Configuration

**If mode.yml is not present environment is set to production**

## El patrón proxy

### Calculador sensación térmica por viento

Un proveedor del clima en Valencia entrega los datos del clima y en base a la temperatura y viento, se calcula la sensación térmica.

### Proxy de cache

Usar el Proxy en esta metáfora permite que la petición al proveedor y el cálculo de la sensación térmica sean cacheados y devueltos por el proxy mientras la caché no haya expirado.

OBSERVACIONES: Hemos dado a la cache un acceso global, mediante un singletón, para que diferentes instancias del proxy, aprovechen los recursos de la cache indistintamente
