require 'gdbm'
require 'json'
require_relative '../config/configuration'

class Cache

  def initialize(database)
    @database = database
  end

  def reset(key)
    gdbm = open_gdbm
    gdbm[key] = '{}'
    gdbm.close
  end

  def set(key, value)
    gdbm = open_gdbm
    cache = {}
    cache['data'] = value
    cache['expired'] = Time.now + Configuration.wind_cache_seconds
    gdbm[key] = cache.to_json
    gdbm.close
  end

  def get(key)
    gdbm = open_gdbm
    cached = JSON.parse(gdbm[key])
    gdbm.close
    return cached['data'] if in_time?(cached['expired'])
    false
  end

  private

  def open_gdbm
    GDBM.new(@database)
  end

  def not_expired?(expiration)
    Time.now.to_s < expiration
  end

  def in_time?(expiration)
    !expiration.nil? && not_expired?(expiration)
  end
end
