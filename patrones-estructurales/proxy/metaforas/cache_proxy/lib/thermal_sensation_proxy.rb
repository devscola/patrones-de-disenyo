require 'thermal_sensation'
require 'cache'

class ThermalSensationProxy

  def initialize(weather_provider)
    database = 'weather.db'
    @cache = Cache.new(database)
    @thermal_sensation = ThermalSensation.new(weather_provider)
  end

  def calculate
    @cache.get('thermal_sensation') || do_cache
  end

  private

  def do_cache
    thermal_sensation = @thermal_sensation.calculate
    @cache.set('thermal_sensation', thermal_sensation)
    thermal_sensation
  end
end
