require 'json'

class WeatherProvider
  KEY = '6b09dfd7d5d8e18f7ec0bab7286878bd'

  def provision
    url = URI("http://api.openweathermap.org/data/2.5/weather?q=Valencia,ES&APPID=#{KEY}&units=metric")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url)
    request["cache-control"] = 'no-cache'
    response = http.request(request)
    JSON.parse(response.read_body)
  end
end
