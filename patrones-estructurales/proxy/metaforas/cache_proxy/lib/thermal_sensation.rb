require 'uri'
require 'net/http'

class ThermalSensation
  def initialize(weather_provider)
    @weather_provider = weather_provider
  end

  def calculate
    provider = @weather_provider.provision
    provider = provider
    (13.12 + (0.6215 * provider['main']['temp']) -
    (11.37 * provider['wind']['speed'] ** 0.16) +
    (0.3965 * provider['main']['temp'] * provider['wind']['speed'] ** 0.16)).round(2)
  end
end
