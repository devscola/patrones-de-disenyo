require 'thermal_sensation_proxy'
require_relative '../config/configuration'
require 'cache'

describe 'La sensación térmica' do
  before(:each) do
    database = 'weather.db'
    cache = Cache.new(database)
    cache.reset('thermal_sensation')
  end
  it 'es calculada a través del proxy' do
    weather_provider = WeatherProviderMock.new
    expected_temperature = 27.6

    thermal_proxy = ThermalSensationProxy.new(weather_provider)

    expect(thermal_proxy.calculate).to eq(expected_temperature)
  end

  it 'es procesada por ThermalSensation sólo una vez si la petición se realiza en menos de un segundo' do
    weather_provider = WeatherProviderMock.new
    expected_temperature = 27.6

    thermal_proxy = ThermalSensationProxy.new(weather_provider)

    expect_any_instance_of(ThermalSensation).to receive(:calculate).once.and_call_original
    thermal_proxy.calculate
    expect(thermal_proxy.calculate).to eq(expected_temperature)
  end

  it 'es calculada de nuevo por ThermalSensation pasado 1 segundo' do
    weather_provider = WeatherProviderMock.new
    expected_temperature = 27.6

    thermal_proxy = ThermalSensationProxy.new(weather_provider)

    expect_any_instance_of(ThermalSensation).to receive(:calculate).twice.and_call_original
    thermal_proxy.calculate
    sleep Configuration.wind_cache_seconds
    expect(thermal_proxy.calculate).to eq(expected_temperature)
  end
  it 'aprovecha los recursos de cache para todas las instancias del proxy' do
    weather_provider = WeatherProviderMock.new
    expected_temperature = 27.6

    thermal_proxy = ThermalSensationProxy.new(weather_provider)
    other_thermal_proxy = ThermalSensationProxy.new(weather_provider)

    expect(weather_provider).to receive(:provision).once.and_call_original
    thermal_proxy.calculate
    expect(other_thermal_proxy.calculate).to eq(expected_temperature)
  end

  it 'es calculada de nuevo por ThermalSensation pasado 1 segundo con diferentes instancias del proxy' do
    weather_provider = WeatherProviderMock.new
    expected_temperature = 27.6

    thermal_proxy = ThermalSensationProxy.new(weather_provider)
    other_thermal_proxy = ThermalSensationProxy.new(weather_provider)

    expect(weather_provider).to receive(:provision).twice.and_call_original
    thermal_proxy.calculate
    sleep Configuration.wind_cache_seconds
    expect(other_thermal_proxy.calculate).to eq(expected_temperature)
  end
end

class WeatherProviderMock

  def provision
    response = {}
    response['main'] = {}
    response['wind'] = {}
    response['main']['temp'] = 25.73
    response['wind']['speed'] = 5.1
    response
  end
end
