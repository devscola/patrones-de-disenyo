# Gestor de Conexiones Wifi

La metáfora consiste en un gestor de conexiones wifi (WifiManager) que provee de una conexión wifi (WifiConnection) a su cliente (Client).

La idea es que el gestor entrega una conexión a su cliente, y este es el que realiza la conexión.

## Proxy

Usar el Proxy en esta metáfora permite al gestor entregar conexiones wifi sin perder el control sobre ellas. Para demostrarlo, hemos hecho que el gestor sea capaz de cambiar la conexión que ha entregado, sin que el cliente se entere, es decir es capaz de hacerlo "en caliente".

## Posibles mejoras

Usar el Proxy tambien nos permite que si la conexión wifi se cae, podriamos intentar rescatar el error en el Proxy, pedirle una nueva conexión al WifiManager, y reintentar la operación sin que el cliente tenga que manejar el error.
