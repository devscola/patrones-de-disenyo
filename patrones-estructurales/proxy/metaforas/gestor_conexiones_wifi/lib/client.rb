class Client
  def initialize(wifi_manager)
    @wifi_manager = wifi_manager
  end

  def connect
    connection = retrieve_wifi_connection()
    connection.connect()
  end

  private

  def retrieve_wifi_connection
    @wifi_manager.retrieve_connection()
  end
end
