require 'wifi_connection'

class WifiManager
  def initialize
    fetch_connections
    prepare_to_connect
  end

  def retrieve_connection
    connection = fetch_connection
    activated_connection = activate(connection)

    activated_connection
  end

  def change_connection
    new_connection = fetch_connection
    active_connection = fetch_active_connection
    active_connection.use(new_connection)

    active_connection
  end

  private

  def fetch_connections
    @available_connections = [
      WifiConnection.new(:one),
      WifiConnection.new(:two),
      WifiConnection.new(:three)
    ]
  end

  def prepare_to_connect
    @active_connection = nil
  end

  def fetch_connection
    return @available_connections.first if @active_connection.nil?

    @available_connections.find { |connection| connection.ssid != @active_connection.ssid }
  end

  def fetch_active_connection
    @active_connection
  end

  def activate(connection)
    @active_connection = Proxy.new
    @active_connection.use(connection)
    @active_connection
  end

  class Proxy
    def use(connection)
      @connection = connection
    end

    def ssid
      @connection.ssid
    end

    def connect
      @connection.connect
    end
  end
end
