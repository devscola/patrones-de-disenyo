class WifiConnection
  def initialize(ssid)
    @ssid = ssid
    @connected = true
  end

  def ssid
    @ssid
  end

  def connect
    @connected
  end
end
