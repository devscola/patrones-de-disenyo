require 'wifi_manager'
require 'wifi_connection'

describe 'WifiManager' do
  it 'devuelve conexiones' do
    manager = WifiManager.new

    connection = manager.retrieve_connection()

    expect(connection).to implement_interface(:ssid, :connect)
  end

  it 'puede cambiar la conexión wifi en cualquier momento' do
    manager = WifiManager.new
    connection = manager.retrieve_connection()
    old_ssid = connection.ssid

    manager.change_connection

    expect(connection.ssid).not_to eq(old_ssid)
  end
end
