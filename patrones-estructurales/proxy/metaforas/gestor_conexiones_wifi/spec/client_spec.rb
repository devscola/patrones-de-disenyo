require 'client'

describe 'Client' do
  it 'usa el wifi manager para conectarse al wifi' do
    wifi_manager = spy('wifi_manager')
    client = Client.new(wifi_manager)

    client.connect()

    expect(wifi_manager).to have_received(:retrieve_connection)
  end
end
