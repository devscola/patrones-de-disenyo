RSpec::Matchers.define :implement_interface do |*interface_methods|
  def obtain_public_methods_of(object)
    exclude_inherited_methods = true
    object.public_methods(exclude_inherited_methods)
  end

  match do |actual|
    implemented_methods = obtain_public_methods_of(actual)
    interface_methods.all? { |method| implemented_methods.include?(method) }
  end

  failure_message do |actual|
    "expected #{actual.class} to implement interface #{interface_methods - obtain_public_methods_of(actual)}"
  end
end
