require 'cash_dispenser'

class CashDispenserProxy
  AUTH_USERS = ["auth_user"]

  def initialize cash_dispenser
    @cash_dispenser = cash_dispenser
  end

  def withdraw(money)
    raise "Not identified user" unless @authorized
    response = @cash_dispenser.withdraw(money)
    submit(response)
  end

  def validate(user)
   @authorized = AUTH_USERS.include?(user)
  end

  private

  def submit response
    @authorized = nil
    response
  end
end
