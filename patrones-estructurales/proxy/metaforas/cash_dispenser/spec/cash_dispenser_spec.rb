require 'cash_dispenser_proxy'

describe 'Proxy CashDispenser' do
  before (:all) do
    @cash_dispenser = CashDispenser.new
    @proxy = CashDispenserProxy.new(@cash_dispenser)
  end

  it 'validates a user authorization' do
    result = @proxy.validate("auth_user")

    expect(result).to eq(true)

    result = @proxy.validate("not_auth_user")

    expect(result).to eq(false)
  end

  it 'allows authorized user to withdraw money' do
    any_amount = 500
    @proxy.validate("auth_user")
    result = @proxy.withdraw(any_amount);

    expect(result).to eq("Here are your $ 500.")
  end

  it 'prevents not authorized users to withdraw money' do
    any_amount = 500
    @proxy.validate("not_auth_user")

    expect{@proxy.withdraw(any_amount)}.to raise_error("Not identified user")
  end

  it 'raise an error if user is not identified' do
    any_amount = 500

    expect{@proxy.withdraw(any_amount)}.to raise_error("Not identified user")
  end

  it 'call the method withdraw of the cash_dispenser it protects' do
    allow(@cash_dispenser).to receive(:withdraw)

    any_amount = 500

    @proxy.validate('auth_user')
    @proxy.withdraw(any_amount)

    expect(@cash_dispenser).to have_received(:withdraw)
  end
end
