# Cash Dispenser

La metáfora consiste en un **cajero automático** que entrega dinero al usuario.

## Proxy

Se aplica un **proxy de protección** (CashDispenserProxy) que controla el acceso al objeto original (CashDispenser) y solo utiliza éste en caso de que el usuario esté autorizado.

Cuando el usuario está autorizado, se hace la petición al objeto original para extraer dinero del cajero. En caso contrario, no se realiza la petición al objeto original y se emite un mensaje de error.

## Posibles mejoras

En el ejemplo, la lista de usuarios autorizados es el array `AUTH_USERS = ["auth_user"]`. En un caso real, sería un servicio de validación.

En lugar de usar el método `validate` tras instanciar el proxy, se podría pasar el usuario como parámetro en el constructor. Para cumplir con los requisitos de proxy, el constructor de la clase original debería recibir también ese parámetro.
