import fetch from 'node-fetch'
import querystring from 'querystring'
import {LocationProviderInterface} from './interfaces'
import {LocationData} from './types'

class LocationProvider implements LocationProviderInterface {
  private url = 'https://nominatim.openstreetmap.org/search/'
  private options = {
    addressdetails: 1,
    format: 'json',
    limit: 1,
    polygon_svg: 1
  }
  public async search(query: string) : Promise<Array<LocationData>> {
    const baseUrl = `${this.url}${query}`
    const params = querystring.stringify(this.options)
    return await this.getData(`${baseUrl}?${params}`)
  }

  private async getData(url: string) {
    const res = await fetch(url)
    const data = await res.json()
    return data
  }
}

export default LocationProvider
