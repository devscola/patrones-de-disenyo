import { LocationProviderInterface } from './interfaces'
import { LocationData } from './types'
import LocationProvider from './LocationProvider'
import Cache from './Cache'

class LocationProviderProxy implements LocationProviderInterface {
  private provider: LocationProvider
  private cache: Cache
  constructor() {
    this.provider = new LocationProvider()
    this.cache = new Cache
  }

  public async search(query: string): Promise<Array<LocationData>> {
    const cachedData = this.cache.retrieve(query)
    
    if (cachedData) {
      return cachedData
    }
    
    const locationsData = await this.provider.search(query)
    this.cache.push(query, locationsData)

    return locationsData
  }

}

export default LocationProviderProxy