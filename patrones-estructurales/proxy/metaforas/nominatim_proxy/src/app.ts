import express from 'express'
import LocationProvider from './LocationProviderProxy'

const app = express()

app.get('/location', async (request, response) => {
  const { city } = request.query
  const locationProvider = new LocationProvider()
  const data = await locationProvider.search(city)

  response.status(200).json(data)
})

export default app
