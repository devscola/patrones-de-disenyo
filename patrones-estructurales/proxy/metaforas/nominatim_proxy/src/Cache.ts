class Cache{
  private cache: {
    [key :string] : any
  }
  constructor(){
    this.cache = {}
  }

  push(key: string, data: any){
    this.cache[key] = data
  }

  retrieve(key: string){
    return this.cache[key]
  }
}

export default Cache