import {LocationData} from './types'

export interface LocationProviderInterface {
  search: (query: string) => Promise<Array<LocationData>>
}