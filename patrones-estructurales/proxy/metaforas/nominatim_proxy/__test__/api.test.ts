import request from 'supertest'

import app from '../src/app'

describe('GET /location', () => {
  it('responds with city data', (done) => {
    request(app)
      .get('/location/?city=cullera')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body[0].address.city).toBe('Cullera')
        done()
      })
  })
})
