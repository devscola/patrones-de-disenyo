import LocationProviderProxy from '../src/LocationProviderProxy'
import LocationProvider from '../src/LocationProvider'
jest.mock('../src/LocationProvider')

describe('LocationProviderProxy', () => {

    // Make typescript accept the use of .mock
    const MockedProvider = <jest.Mock<LocationProvider>>LocationProvider

    beforeEach(() => {
      MockedProvider.mockClear()
    })

    it('provider is only instanced once', async () => {

      const provider = new LocationProviderProxy()
      const query = 'cullera'

      await provider.search(query)
      await provider.search(query)

      expect(MockedProvider).toHaveBeenCalledTimes(1)

    })

    it('real objects search is called only once per unique query', async () => {

      const mockedResponse = [{"place_id":198023660,"licence":"Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright","osm_type":"relation","osm_id":343971,"boundingbox":["39.1033007","39.2253945","-0.308939","-0.2163899"],"lat":"39.1647217","lon":"-0.2542313","display_name":"Cullera, la Ribera Baixa, Valencia, Valencian Community, 46408, Spain","class":"boundary","type":"administrative","importance":0.585595746918757,"icon":"https://nominatim.openstreetmap.org/images/mapicons/poi_boundary_administrative.p.20.png","address":{"city":"Cullera","region":"la Ribera Baixa","county":"Valencia","state":"Valencian Community","postcode":"46408","country":"Spain","country_code":"es"}}]

      const provider = new LocationProviderProxy()
      const query = 'cullera'
      const anotherQuery = 'sueca'
      const mockLocationProviderInstance = MockedProvider.mock.instances[0];

      MockedProvider.prototype.search.mockReturnValue(mockedResponse)

      await provider.search(query)
      await provider.search(query)

      expect(mockLocationProviderInstance.search).toHaveBeenCalledTimes(1)

      await provider.search(anotherQuery)

      expect(mockLocationProviderInstance.search).toHaveBeenCalledTimes(2)

    })
})
