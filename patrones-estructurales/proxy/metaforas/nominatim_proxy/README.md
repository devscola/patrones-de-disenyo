# Nominatim Proxy

La metáfora consiste en una API REST que consume un servicio de terceros ([Nominatim](https://nominatim.openstreetmap.org/))

La idea es controlar las llamadas al servicio externo mediante el uso de un proxy virtual que cachee las respuestas.

## Proxy

Usar el Proxy en esta metáfora permite a la API cachear los datos del servicio externo para únicamente realizar llamadas externas si la query cambia y así evitar llamadas repetidas.

## Posibles mejoras

Podríamos almacenar el caché en un repositorio para persistir los datos.
