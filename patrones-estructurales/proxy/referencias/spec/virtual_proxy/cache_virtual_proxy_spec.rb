require 'virtual_proxy/cache_virtual_proxy'
require 'real'

describe 'Cache Proxy' do
  it 'delega las llamadas al objeto real' do
    proxy = CacheVirtualProxy.new

    expect(proxy.execute).to eq 'some_data'
  end

  it 'sólo la primera llamada al objeto real es procesada por este' do
    proxy = CacheVirtualProxy.new

    expect_any_instance_of(Real).to receive(:execute).once.and_call_original

    proxy.execute
    proxy.execute
  end
end
