require 'virtual_proxy/lazy_virtual_proxy'
require 'real'

describe 'Lazy Proxy' do
  it 'instancia el objeto real cuando lo necesita' do
    proxy = LazyVirtualProxy.new

    expect(Real).to receive(:new).and_call_original

    proxy.execute
  end

  it 'no se instancia más de una vez' do
    proxy = LazyVirtualProxy.new

    expect(Real).to receive(:new).once.and_call_original

    proxy.execute
    proxy.execute
  end

  it 'no se inicializa al instanciar el proxy' do
    expect(Real).not_to receive(:new)

    LazyVirtualProxy.new
  end
end
