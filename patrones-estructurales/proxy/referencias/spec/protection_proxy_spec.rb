require 'real'
require 'protection_proxy'

RSpec::Matchers.define :implement_interface do |expected|
  match do |actual|
    (actual.public_methods & expected.public_methods) == expected.public_methods
  end
end

RSpec.describe 'Proxy de Protección' do

  it 'delega en el objeto real' do
    proxy = ProtectionProxy.new

    expect_any_instance_of(Real).to receive(:execute)

    proxy.execute
  end

  it 'controla el acceso al objeto real' do
    proxy = ProtectionProxy.new
    proxy.deny_access

    expect{
      proxy.execute
    }.to raise_error('access denied')
  end

  it 'cumple la misma interfaz que el objeto real' do
    proxy = ProtectionProxy.new
    real_object = Real.new

    expect(proxy).to implement_interface(real_object)
  end
end
