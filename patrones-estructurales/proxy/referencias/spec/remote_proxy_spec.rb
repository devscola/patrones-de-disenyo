require 'remote_proxy'
require 'http_client'

RSpec.describe 'Proxy Remoto' do
  class TestHttpClient < HttpClient
    def post
      :some_data
    end
  end

  it 'Usa un cliente http para responder a las llamadas del objeto remoto' do
    expected_data = :some_data
    proxy = RemoteProxy.new(TestHttpClient.new)

    data = proxy.serialize

    expect(data).to eq(expected_data)
  end
end
