require 'real'

class LazyVirtualProxy
  def execute
    acquire unless is_acquired_real_object?
    @real.execute
  end

  private

  def acquire
    @real = Real.new
  end

  def is_acquired_real_object?
    !@real.nil?
  end
end