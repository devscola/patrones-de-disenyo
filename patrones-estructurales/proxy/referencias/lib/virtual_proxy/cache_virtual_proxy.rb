require 'real'

class CacheVirtualProxy
  def initialize
    @real = Real.new
    @cache = {}
  end

  def execute
    if !cached?(:execute)
      set(:execute, @real.execute)
    end

    get(:execute)
  end

  private

  def set(key, value)
    @cache[key] = value
  end

  def get(key)
    @cache[key]
  end

  def cached?(key)
    !@cache[key].nil?
  end
end