class ProtectionProxy
  def initialize
    @real = Real.new
    @access = true
  end

  def execute
    check_access!

    @real.execute
  end

  def deny_access
    @access = false
  end

  private

  def check_access!
    raise 'access denied' unless @access
  end
end
