class RemoteProxy
  def initialize(http_client)
    @http_client = http_client
  end

  def serialize
    response = @http_client.post()

    response
  end
end
