class Factory {
  static city (type) {
    const result = {
      'valencia': new ValenciaIRPF,
      'madrid': new MadridIRPF
    }
    return result[type]
  }
}

class ImpementationInterface {
  calculateSalary () {
    throw new Error('calculateSalary method not implemented')
  }
}

class MadridIRPF extends ImpementationInterface {
  constructor () {
    super()
  }
  calculateSalary () {
    return false
  }
}

class ValenciaIRPF extends ImpementationInterface {
  constructor () {
    super()
  }

  calculateSalary () {
    return true
  }

  getCity () {
    return 'Valencia'
  }
}

class AbstractionInterface {
  getSalary () {
    throw new Error('getSalary method not implemented')
  }
}

class SalaryCalculator extends AbstractionInterface {
  constructor (irpfImplementation) {
    super()
    this.irpfPlace = irpfImplementation
    if (typeof(irpfImplementation) == "string"){
      this.irpfPlace = Factory.city(irpfImplementation)
    }
  }

  getSalary () {
    return this.irpfPlace.calculateSalary()
  }
}

module.exports = {
  Factory,
  ImpementationInterface,
  MadridIRPF,
  ValenciaIRPF,
  AbstractionInterface,
  SalaryCalculator
}
