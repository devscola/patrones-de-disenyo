const {
  Factory,
  ImpementationInterface,
  MadridIRPF,
  ValenciaIRPF,
  AbstractionInterface,
  SalaryCalculator
} = require("../irpf.js");


describe('IRPFBridge', () => {
  it('La implementacion debe implementar su interfaz', () => {
    const irpfImplementation = new ValenciaIRPF()

    expect(irpfImplementation.calculateSalary()).toBeDefined()
  })
  it('La abstracción debe implementar su interfaz', () => {
    const salaryAbstraction = new SalaryCalculator("valencia")

    expect(salaryAbstraction.getSalary()).toBeDefined()
  })
  it('La interfaz de la implementacion no tiene por que ser igual que la interfaz de la Abstraction', () => {
    const salaryAbstraction = new SalaryCalculator("valencia")
    const valenciaImplementation = salaryAbstraction.irpfPlace;

    expect(salaryAbstraction.getSalary()).toBeDefined()
    expect(() => {
      valenciaImplementation.getSalary()
    }).toThrow(TypeError)
  })
  it('La implementación debe ser seleccionada en tiempo de ejecución', () => {
    const salaryAbstraction = new SalaryCalculator("valencia")
    const valenciaImplementation = salaryAbstraction.irpfPlace;

    expect(valenciaImplementation).toBeInstanceOf(ValenciaIRPF)
  })
  it('La abstracción ejecuta las peticiones de los clientes de la implementación concreta', () => {
    const salaryAbstraction = new SalaryCalculator("madrid")
    const madridImplementation = salaryAbstraction.irpfPlace;
    madridImplementation.calculateSalary = jest.fn()
    salaryAbstraction.getSalary()

    expect(madridImplementation.calculateSalary).toBeCalled()
  })

  it('Abstracción e implementación pueden ser desarrolladas independientemente', () => {
    const salaryAbstraction = new SalaryCalculator("valencia")
    const valenciaImplementation = salaryAbstraction.irpfPlace;

    expect(valenciaImplementation.calculateSalary()).toBeDefined()
    expect(valenciaImplementation.getCity()).toBeDefined()
    expect(salaryAbstraction.getSalary()).toBeDefined()
    expect(() => {
      salaryAbstraction.getCity()
    }).toThrow(TypeError)
  })

  it('La Abstracción puede decidir que implementación instanciar en el constructor dependiendo de los parámetros que recibe a través del constructor', ()=>{
    const salaryAbstraction = new SalaryCalculator("valencia");

    expect(salaryAbstraction.getSalary()).toBe(true);
  })
})
