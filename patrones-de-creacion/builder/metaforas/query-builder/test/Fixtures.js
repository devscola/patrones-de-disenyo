const getDB = require('../infrastructure/database').getDB
class Fixtures {
  static async fill () {
    const db = await getDB()
    await db.collection('pets')
      .insertMany(Fixtures.data())
      .then(result => result)
      .catch(error => {
        throw error
      })

  }
  static async flush () {
    const db = await getDB()
    console.log('Database flushed: ', await db.dropDatabase())
  }

  static data () {
    const pets = [
      {
        'name': 'Ahcul',
        'age': 11,
        'size': 'medium',
        'character': 'faithful'
      },
      {
        'name': 'Drako',
        'age': 2,
        'size': 'medium',
        'character': 'family'
      },
      {
        'name': 'Rayo',
        'age': 3,
        'size': 'small',
        'character': 'Children friendly'
      },
      {
        'name': 'Zhur',
        'age': 6,
        'size': 'small',
        'character': 'Crazy'
      }
    ]
    return pets
  }
}

module.exports = Fixtures
