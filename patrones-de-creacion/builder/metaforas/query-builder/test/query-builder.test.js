const PetsQueryBuilder = require('../src/PetsQueryBuilder')
const Collection = require('../src/Collection')
const Fixtures = require('./Fixtures')

beforeAll(async () => {
  await Fixtures.fill()
})

afterAll(async () => {
  await Fixtures.flush()
})

describe('Query builder', () => {
  it('can filter match queries', async () => {
    const quantityExpected = 1
    const filters = [{
      field: 'name',
      query: 'match',
      value: 'Ahcul'
    }]

    const query = new PetsQueryBuilder(filters)
    const filter = query.build()
    const response = await Collection.filter(filter.query())

    expect(response.length).toBe(quantityExpected)
    expect(response[0].name).toBe('Ahcul')
  })

  it('can filter lower than queries', async () => {
    const quantityExpected = 1
    const filters = [{
      field: 'age',
      query: 'lowerThan',
      value: 3
    }]

    const query = new PetsQueryBuilder(filters)
    const filter = query.build();
    const response = await Collection.filter(filter.query())

    expect(response.length).toBe(quantityExpected)
    expect(response[0].age).toBe(2)
    expect(response[0].name).toBe('Drako')
  })

  it('can filter greater than queries', async () => {
    const quantityExpected = 1
    const filters = [{
      field: 'age',
      query: 'greaterThan',
      value: 7
    }]

    const query = new PetsQueryBuilder(filters)
    const filter = query.build();
    const response = await Collection.filter(filter.query())

    expect(response.length).toBe(quantityExpected)
    expect(response[0].age).toBe(11)
    expect(response[0].name).toBe('Ahcul')
  })

  it('can filter result in mached array queries', async () => {
    const quantityExpected = 2
    const filters = [{
      field: 'character',
      query: 'inArray',
      value: ['faithful', 'family']
    }]

    const query = new PetsQueryBuilder(filters)
    const filter = query.build();
    const response = await Collection.filter(filter.query())

    expect(response.length).toBe(quantityExpected)
    expect(response[0].name).toBe('Ahcul')
    expect(response[0].character).toBe('faithful')
    expect(response[1].name).toBe('Drako')
    expect(response[1].character).toBe('family')
  })

  it('can filter with more than one filter query', async () => {
    const quantityExpected = 1
    const filters = [{
      field: 'age',
      query: 'lowerThan',
      value: 4
    },
    {
      field: 'size',
      query: 'match',
      value: 'small'
    }]

    const query = new PetsQueryBuilder(filters)
    const filter = query.build();
    const response = await Collection.filter(filter.query())

    expect(response.length).toBe(quantityExpected)
    expect(response[0].age).toBe(3)
    expect(response[0].name).toBe('Rayo')
  })
})
