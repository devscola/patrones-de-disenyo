const getDB = require('../infrastructure/database').getDB

class Collection {
  static async filter (query) {
    const db = await getDB()
    const result = await db.collection('pets')
      .find(query).toArray()
    return result
  }
}

module.exports = Collection
