class PetsQueryBuilder {
  constructor (filters) {
    this.filters = filters
  }

  build () {
    const query = new Query()
    this.filters.forEach(filter => {
      query.add(filter)
    })
    return query
  }
}

class Query {
  constructor () {
    this.filter = {}
  }

  add (filter) {
    const map = {
      'match': this.whenExactMatch,
      'lowerThan': this.whenLowerThan,
      'greaterThan': this.whenGreaterThan,
      'inArray': this.whenContains,
    }
    const callback = map[filter.query].bind(this)
    callback(filter)
  }

  whenExactMatch(filter){
    this.filter[filter.field] = filter.value
  }

  whenLowerThan(filter){
    this.filter[filter.field] = {$lt: filter.value}
  }

  whenGreaterThan(filter){
    this.filter[filter.field] = {$gt: filter.value}
  }

  whenContains(filter){
    this.filter[filter.field] = {$in: filter.value}
  }

  query () {
    return this.filter
  }
}

module.exports = PetsQueryBuilder
