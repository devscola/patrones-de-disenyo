const Hamburguer = require('./Hamburguer')

class HamburguerBuilder {
  build(){
    this.hamburguer = new Hamburguer(this.patty)
    if(this.pickle) this.hamburguer.addPickle()
    if(this.tomato) this.hamburguer.addTomato()

    return this.hamburguer
  }

  addPatty(type = 'Sandwich'){
    this.patty = type
    return this
  }

  addPickle(){
    this.pickle = true
    return this
  }

  addTomato(){
    this.tomato = true
    return this
  }
}

module.exports = HamburguerBuilder
