class Hamburguer{
  constructor(patty){
    this.bun = true
    this.patty = patty
    this._checkRequiredParams()
  }

  addPickle(){
    this.pickle = true
  }

  _checkRequiredParams(){
    if(!this.patty) this.patty = 'Cereal'
  }

  addTomato(){
    this.tomato = true
  }
}

module.exports = Hamburguer
