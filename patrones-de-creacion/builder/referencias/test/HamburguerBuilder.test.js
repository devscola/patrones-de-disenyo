const HamburguerBuilder = require('./hamburger/HamburguerBuilder')
const Hamburguer = require('./hamburger/Hamburguer')

describe('HamburguerBuilder', () => {
  it('Encapsula la construcción de un objeto complejo al cliente', () => {
    const hamburguer = new HamburguerBuilder().addPatty().addPickle().build()

    expect(hamburguer).toBeInstanceOf(Hamburguer)
  })

  it('Permite construir diferentes representaciones del mismo tipo', () => {
    const hamburguerWithPickle = new HamburguerBuilder().addPatty().addPickle().build()
    const hamburguerWithoutPickle = new HamburguerBuilder().addPatty().build()

    expect(hamburguerWithPickle).toBeInstanceOf(Hamburguer)
    expect(hamburguerWithPickle.pickle).toBe(true)
    expect(hamburguerWithoutPickle).toBeInstanceOf(Hamburguer)
    expect(hamburguerWithoutPickle.pickle).toBeUndefined()
  })

  it('Garantiza la creación del objeto completo (con las invariantes de clase)', () => {
    const hamburguer = new HamburguerBuilder().build()

    expect(hamburguer.patty).toBe('Cereal')
  })

  it('Permite la construcción de un objeto sin un orden predeterminado, retrasando su creación hasta que tenga todo lo necesario', () => {
    const ordered = new HamburguerBuilder().addPatty().addTomato().addPickle().build()
    const notOrdered = new HamburguerBuilder().addPickle().addPatty().addTomato().build()

    expect(ordered).toMatchObject(notOrdered)
  })

  it('Permite crear un objeto en diferentes fases de la ejecución', () => {
    const finishedHamburguer = new HamburguerBuilder().addPatty().addTomato().addPickle().build()
    const hamburguerBuilder = new HamburguerBuilder().addPatty()
    const unfinishedHamburguer = hamburguerBuilder.build()

    expect(unfinishedHamburguer).not.toMatchObject(finishedHamburguer)

    const hamburguer = hamburguerBuilder.addTomato().addPickle().build()

    expect(hamburguer).toMatchObject(finishedHamburguer)
  })

  it('Permite crear objetos con valores por defecto definidos en el builder', () => {
    const hamburguer = new HamburguerBuilder().addPatty().build()

    expect(hamburguer.patty).toBe('Sandwich')
  })
})
